package ju;


public class Session 
{
	   String sessionId;
	   String userName;
	   String password;
	   
	   public void setSessionId(String sessionId)
	   {
		   this.sessionId = sessionId;
	   }
	   
	   public void setUserName(String userName)
	   {
		   this.userName = userName;
	   }
	   
	   public void setPassword(String password)
	   {
		   this.password = password;
	   }
	   
	   public String getSessionId()
	   {
		   return sessionId;
	   }
	   
	   public String getUserName()
	   {
		   return userName;
	   }
	   
	   public String getPassword()
	   {
		   return password;
	   }
}
