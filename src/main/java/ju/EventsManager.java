package ju;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * EventsManager used to manage events throughout the test life cycle.
 * Manages what events to use for offer submission.
 * Manages what events need to be graded & paid out.
 */  
public class EventsManager 
{
	static CopyOnWriteArrayList<Event> events = new CopyOnWriteArrayList<Event>();
	static List<Event> expiredEvents = Collections.synchronizedList(new ArrayList<Event>());
	static Random random = new Random();
	
	public static void addEvent(Event event)
	{
		// Add open/active event to events.
		events.add(event);
	}
	
	public static void removeEvent(Event event)
	{
		// Removes active/open event.
		events.remove(event);
	}
	
	public static void clear()
	{
		// Clears active/open events list.
		events.clear();
	}
	
	public static List<Event> getEvents()
	{
		// Return list of active/open events.
		return events;
	}
	
	public static int getEventsSize()
	{
		// Return number of active/open events.
		return events.size();
	}
	
	public Event getEvent(Long id)
	{
		// Get event by event id.
		for(Event event : events) 
		{
			if(event.getId().equals(id))
		    {
				return event;
		    }		    		
		}
		return null;
	}
	
	// Dont think this is used  //
	public Event getEventbySport(Long id)
	{
		// Get event by sport id.
		for(Event event : events) 
		{
			if(event.getId().equals(id))
		    {
				return event;
		    }		    		
		}
		return null;
	}
	
	public static int getTotalNumberOfRunners()
	{
		// Return total number of active/open runners.
		int totalNumberOfRunners = 0;
		for(Event event : events) 
		{
			totalNumberOfRunners += event.getNumberOfRunnersForEvent();
		}
		return totalNumberOfRunners;
	}
	
	public static int getTotalNumberOfMarkets()
	{
		// Return total number of active/open markets.
		int totalNumberOfMarkets = 0;
		for(Event event : events) 
		{
			totalNumberOfMarkets += event.getNumberOfMarkets();
		}
		return totalNumberOfMarkets;
	}
	
	public static Event getRandomEvent()
	{
		Event event = events.get(random.nextInt(events.size()));
		   
		return event;		
	}
	
	public static long getRandomRunnerId()
	{
		Event event = events.get(random.nextInt(events.size()));
		return event.getRandomRunnerId();
		
	}
	
	public static long getRandomEventId()
	{
		Event event = events.get(random.nextInt(events.size()));
		return event.getId();	
	}
	
/*--------------------*** Expired Events ***--------------------*/
	
	public static void setEventsToExpired()
	{
		// Checks if any event startTime has expired and sets event to expired. 
		// Gets events marked expired and adds to expiredEvents List.
		// Removes expired event from events list

		for(Event event : events) 
		{
			if(event.checkEventStartTimeHasExpired())
			{
				events.remove(event);
				expiredEvents.add(event);
			}
		}
	}
	
	public static List<Event> getExpiredEvents()
	{
		// Gets events marked expired..
		// This list is then used for closing, grading and paying out event.

		return expiredEvents;
	}	
		
	public static void removeExpiredEvent(Event event)
	{
		expiredEvents.remove(event);
	}
	   
}