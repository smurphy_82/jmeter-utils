package ju;

public final class MarketTypes {
	
	private MarketTypes()
	{
		
	}
	
	public static final String MONEY_LINE = "MONEY_LINE";
	public static final String ONE_X_TWO = "ONE_X_TWO";
	public static final String HANDICAP = "HANDICAP";
	public static final String TOTAL =	"TOTAL";
	public static final String BOTH_TO_SCORE = null;
	public static final String CORRECT_SCORE = "CORRECT_SCORE";
	public static final String HALF_TIME_FULL_TIME = null;
	public static final String TOURNAMENT_MATCH_UP = "TOURNAMENT_MATCH_UP";	
	public static final String ROUND_MATCH_UP = "ROUND_MATCH_UP";
	public static final String TO_QUALIFY = null;
	public static final String OUTRIGHT_DED_FACT = "OUTRIGHT_DED_FACT";
	public static final String PLACE_DED_FACT = "OUTRIGHT_PLACE_FACT";
}
