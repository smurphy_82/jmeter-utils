package ju;
import java.util.*;

public class Market 
{
	ArrayList<Long> runners = new ArrayList<Long>();
	Random random = new Random();
	Long id;
	String marketType;	
	String status;
	int numberOfWinners = 1;
		
   public Market(Long marketId, String marketType)
   {
	   this.id = marketId;
	   this.marketType = marketType;
	   this.status = "OPEN";   
   }
   
   public Market(Long marketId)
   {
	   this.id = marketId;
	   this.status = "OPEN";
   }
 
   public void setStatusToClose()
   {
	   this.status = "CLOSE";
   }
   
   public void setRunner(long runnerId)
   {
	   this.runners.add(runnerId); 
   }
   
   public void updateNumberOfWinners(int numberOfWinners)
   {
	   this.numberOfWinners = numberOfWinners;
   }
   
   public Long getId()
   {
	   return id; 
   }
   
   public String getMarketType()
   {
	   return this.marketType; 
   }
   
   public Long getRandomRunner()
   {
	   Long runnerId = runners.get(random.nextInt(runners.size()));
	   return runnerId; 
   }
   
   public Long getRunnerId(int index)
   {
	   Long runnerId = runners.get(index);
	   return runnerId; 
   }
   
   public int getNumberOfWinners()
   {
	  return this.numberOfWinners;
   }
   
   public int getNumberOfRunners()
   {
	   return runners.size();
   }
   
   public String getStatus()
   {
	   return this.status;
   }
      
}