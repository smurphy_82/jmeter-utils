package ju;

public final class HorseRacingLocations {
	
	private HorseRacingLocations()
	{
			
	}
		
	public static final String KEMPTON = "# Kempton";
	public static final String SOUTHWELL = "# Southwell";
	public static final String PLUMTON = "# Plumton";
}
