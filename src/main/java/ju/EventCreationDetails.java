package ju;
import java.text.*;
import java.util.*;

/*
 * Creates event details required for event creation
 */  
public class EventCreationDetails 
{
   ArrayList<Long> participants = new ArrayList<Long>();
   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00.000'Z'");
   SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
   RunnerHelper rh = new RunnerHelper();
   Integer startTimeOffset;
   Integer startTimeIncrement;
   String displayTime;
   String startDate;
   String displayDate;
   Integer numberOfEvents;
   Integer numberOfWinners;
   Integer numberOfRunners;
   String fullName;
   
   public EventCreationDetails()

   {	   
 	   
   }
    
   public void setStartDate(Integer rawStartOffSet)
   {
	   this.startDate = (String) sdf.format(new Date(System.currentTimeMillis() + rawStartOffSet));
   }
   
   public void setDisplayTime(Integer rawStartOffSet)
   {
	   this.displayTime = (String) tf.format(new Date(System.currentTimeMillis() + rawStartOffSet));
   }
      
   public void setDisplayDate()
   {
	   this.displayDate = (String) sdf.format(new Date());
   }
      
   public void setNumberOfRunners()
   {
	   this.numberOfRunners = rh.runnerDistribution();
   }
   
   public void setNumberOfRunners(Integer numberOfRunners)
   {
	   this.numberOfRunners = numberOfRunners;
   }
   
   public void setNumberOfWinners()
   {
	   this.numberOfWinners = rh.getNumberOfWinners(this.numberOfRunners);
   }
   
   public void setNumberOfWinners(Integer numberOfWinners)
   {
	   this.numberOfWinners = numberOfWinners;
   }
   
   public void setName(String name)
   {
	   this.fullName = name;
   }
   
   public void setParticipantId(Long participantId)
   {
	   this.participants.add(participantId);
   }
  
   public String getStartDate()
   {
	  return this.startDate;
   }
 
   public String getDisplayTime()
   {
	  return this.displayTime;
   }
 
   public String getDisplayDate()
   {
	  return this.displayDate;
   }
   
   public Integer getNumberOfRunners()
   {
	  return this.numberOfRunners;
   }
   
   public Integer getNumberOfWinners()
   {
	  return this.numberOfWinners;
   }
   
   public String getName()
   {
	  return this.fullName;
   }
   
   public Long getParticipantId(int index)
   {
	   return participants.get(index);
   }
   
}