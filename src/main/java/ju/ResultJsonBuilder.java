package ju;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultJsonBuilder {
	
	String message;
	
	
	public String build(int numberOfWinners, Event event)
	{
		JSONArray jsonArray = new JSONArray();
		for(int result = 1; result <= numberOfWinners; result++)
		{
			ArrayList<Long> eventParticipantWinners = event.getEventParticipants(numberOfWinners);
			JSONObject jb = this.buildResultObject(result, eventParticipantWinners.get(result-1));
			jsonArray.put(jb);
			
		}
		message = jsonArray.toString();
		return message;
	}
	
	public JSONObject buildResultObject(int result, Long eventparticipant)
	{
		JSONObject jsonObject = new JSONObject();
		JSONArray resultsArray = new JSONArray();
		JSONObject resultsObject = new JSONObject();
		try {
			resultsObject.put("period", "FULL_EVENT");
			resultsObject.put("result", result);
			jsonObject.put("event-participant-id", eventparticipant);
		
			resultsArray.put(resultsObject);
			jsonObject.put("results", resultsArray);
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonObject;
		
	}

}
