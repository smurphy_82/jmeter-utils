package ju;
import java.util.*;

public class Offer 
{
   long eventId;
   long marketId;
   long runnerId;
   long offerId;
   double odds;
   double stake;
   double availableAmount;
   List<String> sides_binary = Arrays.asList("win");
   List<String> sides_backlay = Arrays.asList("back", "lay"); 
   List<String> sides = Arrays.asList("win", "lose", "back", "lay");
   String side;
   String oddsType;
   String exchangeType;
   String currency;
   Random random = new Random();

   public void setOfferId(long offerId)
   {
	   this.offerId = offerId;
   }
   
   public void setRunnerId(long runnerId)
   {
	   this.runnerId = runnerId;
   }
   
   public void setEventId(long eventId)
   {
	   this.eventId = eventId;
   }
   
   public void setMarketId(long marketId)
   {
	   this.marketId = marketId;
   }   
   
   public void setAvailableAmount(double availableAmount)
   {
	   this.availableAmount = availableAmount;
   }
   
   public void setSide(String side)
   {
	   this.side = side;
   }
   
   public void setOdds(double odds)
   {
	   this.odds = odds;
   }
   
   public void setOddsType(String oddsType)
   {
	   this.oddsType = oddsType;
   }
   
   public void setStake(double stake)
   {
	   this.stake = stake;
   }
   
   public void setExchangeType(String exchangeType)
   {
	   this.exchangeType = exchangeType;
   }  
   
   public void setCurrency(String currency)
   {
	   this.currency = currency;
   }
   
   public long getOfferId()
   {
	   return offerId;
   }
   
   public long getRunnerId()
   {
	   return runnerId;
   }
   
   public long getEventId()
   {
	   return eventId;
   }
   
   public long getMarketId()
   {
	   return marketId;
   }   
   
   public double getAvailableAmount()
   {
	   return availableAmount;
   }
   
   public String getSide()
   {
	   return side;
   }
   
   public double getOdds()
   {
	   return odds;
   }
   
   public String getOddsType()
   {
	   return oddsType;
   }
   
   public double getStake()
   {
	   return stake;
   }
   
   public String getExchangeType()
   {
	   return exchangeType;
   }
   
   public String getCurrency()
   {
	   return currency;
   }
   
   public void matchOffer()
   {
	   
	   int index = random.nextInt(2);  
	   
	   if(this.availableAmount < 200.00)
	   {
		   this.stake = this.availableAmount;		   
	   }
	   else
	   {
		   if(index < 1)
		   {
			   this.stake = this.availableAmount/3;
		   }
		   else
		   {
			   this.stake = this.availableAmount;
		   }
	   }	   
   }
        
   public Offer(String exchangeType)
   {
	   if (exchangeType.toLowerCase().equals("binary")){
		   this.side = sides_binary.get(random.nextInt(sides_binary.size()));
	   }else{
		   this.side = sides_backlay.get(random.nextInt(sides_backlay.size()));
	   }
	   
	   this.stake  = Math.round(((Math.random() * 100) + 2)*100)/100;
	   this.odds = (Math.random() * 10) + 100;   
   }
   
   public Offer()
   {

	   this.side = sides.get(random.nextInt(sides.size()));
	   this.stake  = Math.round(((Math.random() * 100) + 2)*100)/100;
	   this.odds = (Math.random() * 10) + 100;   
   }   
   
}