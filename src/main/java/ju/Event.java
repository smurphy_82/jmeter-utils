package ju;
import ju.Market;

import java.text.ParseException;
import java.text.*;
import java.util.*;

public class Event 
{
   
   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
   HashMap<Long, ArrayList<Long>> runners = new HashMap<Long, ArrayList<Long>>();
   ArrayList<Long> eventParticipants = new ArrayList<Long>();
   List<Long> participants = new ArrayList<Long>();
   ArrayList<Market> markets = new ArrayList<Market>();
   ArrayList<Long> marketIds = new ArrayList<Long>();
   Long epochCurrentTime = System.currentTimeMillis();
   Random random = new Random();
   long id;
   Market market;
   Long sportId;
   String name;    
   String startDate;
   String displayDate;
   Long startTime;
   Boolean expiredFlag;  
   int numberOfRunners;
   
// Event Constructors
   public Event(Long id, Long sportId)
   {
	   this.id = id;
	   this.sportId = sportId;
	   this.expiredFlag = false;
   }
   
   public Event(String name, Long sportId)
   {
	   this.name = name;
	   this.sportId = sportId;
	   this.expiredFlag = false;
   }
    
// Event Setters   
   
   public void setEventId(long id)
   {
	   this.id = id;
   }
   
   public void setSportId(Long sportId)
   {
	   this.sportId = sportId;
   }
   
   public void setNumberOfRunners(int numberOfRunners)
   {
	   this.numberOfRunners = numberOfRunners;
   }
   
   public void setMarket(long marketId, String marketType)
   {
	   Market market = new Market(marketId, marketType);
	   this.market = market;
	   this.addMarketToListOfMarkets(this.market);
   }
   
   public void setRunnerForMarket(long runnerId)
   {
	   this.market.setRunner(runnerId);   
   }
   
   public void setEventParticipant(long eventParticipantId)
   {
	   if (!this.getSportId().equals(24735152712200l) && eventParticipants.size() >= 2)
	   {
		   //No need to keep adding the same eventParticpants for none Horse Racing events
		   assert true; 
	   }
	   else
	   {
	   eventParticipants.add(eventParticipantId);
	   }
   }
   
   public void setEventStartDate(String startDate)
   {
	   // Sets the string startDate.
	   // Sets the epoch startTime.
	   this.startDate = startDate;	
	   this.setEventStartTimeEpoch(this.startDate);
   }
    
   public void setDisplayDate(String displayDate)
   {	  
	   this.displayDate = displayDate;
   }
   
   public void setEventStartTimeEpoch(String startDate)
   {
	   try {
		   Date st = (Date) sdf.parse(startDate);
		   this.startTime = st.getTime();
			} catch (ParseException e) {
			e.printStackTrace();
		}	   
   }
   
   public void setNumberOfWinnersForMarket()
   {
	   if(this.market.getMarketType().equals("place_ded_fact"))
	   {
		   RunnerHelper rh = new RunnerHelper();
		   this.market.updateNumberOfWinners(rh.getNumberOfWinners(this.market.getNumberOfRunners()));
	   }
   }
   
 //////////////////////////////////////////////////////////////////////////////////////////////////////////  
    
   public void addMarketToListOfMarkets(Market market)
   {
	   this.markets.add(market);
   }  
     
   public boolean checkEventStartTimeHasExpired()
   {
	   Long timeToStart;
	   Long epochCurrentTime = System.currentTimeMillis();
	   timeToStart = this.startTime - epochCurrentTime;
	   if (timeToStart < 0)
	   {
		   expiredFlag = true;
	   }
	   else
	   {
		   expiredFlag = false;
	   }
	   return expiredFlag;
   }
   
   public Long getId()
   {
	   return this.id;
   }
   
   public int getNumberOfRunnersForEvent()
   {	   
	   int eventRunners = 0;
	   for(Market market : markets) 
	   {
		   eventRunners += market.getNumberOfRunners();
	   }	   
	   return eventRunners;
   }
   
   public int getNumberOfMarkets()
   {   
	   return markets.size();
   }
      
   public List<Long> getEventParticipants()
   {
	   return eventParticipants;
   }
   
   public Long getRandomEventParticipant()
   {
	   return eventParticipants.get(random.nextInt(eventParticipants.size()));
   }
   
   public ArrayList<Long> getEventParticipants(int numberOfEventParticipants)
   {
	   ArrayList<Long> ep = new ArrayList<Long>();
	   while(ep.size() != numberOfEventParticipants)
	   {
		   Long epId = this.getRandomEventParticipant();
		   if(!ep.contains(epId))
		   {
			   ep.add(epId);
		   }		   
	   }
	   return ep;
   }
      
   public List<Long> getParticipants()
   {
	   return participants;
   }
      
   public Long getSportId()
   {
	   return sportId;
   }
   
   public boolean getExpiredFlag()
   {
	   return expiredFlag;
   }
     
   public long getRandomRunnerId()
   {
	   Market market = markets.get(random.nextInt(markets.size()));
	   long runnerId = market.getRandomRunner();
	   return runnerId;
   }
   
   public long getRandomMarketId()
   {
	   Market market = markets.get(random.nextInt(markets.size()));
	   
	   return market.id;
   }
   
   public Market getRandomMarket()
   {
	   Market market = markets.get(random.nextInt(markets.size()));
	   
	   return market;
   }
   
   public Market getOpenMarket()
   {
	   Market openMarket = null;
	   for(Market market : markets) 
	   {
		   if(market.getStatus().equals("OPEN"))
		   {
			   openMarket = market;
		   }
	   }
	   return openMarket;
   }
   
   public ArrayList<Long> getMarketIds()
   {
	   
	   for(Market market : markets) 
	   {
		   marketIds.add(market.getId());
	   }
	    return marketIds;
   } 
   
   public Long getRandomRunnerIdByMarket(long MarketId)
   {
	   Long runnerId = null;
	   for(Market market : markets) 
	   {
	        if(market.getId().equals(MarketId)) 
	        {
	        	runnerId = market.getRandomRunner();
	        }
	    }
	    return runnerId;
	}
   
   public int getMaxNumberOfWinnersforEvent()
   {
	   int maxNumberOfWinners = 1;
	   for(Market market: markets)
	   {
		 int numberOfWinners = market.getNumberOfWinners();
		 if(maxNumberOfWinners <= numberOfWinners)
		 {
			 maxNumberOfWinners = numberOfWinners;
		 }	 
	   }
	   return maxNumberOfWinners;
   }
}