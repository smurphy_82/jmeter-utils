package ju;
import java.util.Random;

public class RunnerHelper {
	
	Random random = new Random();
	
	
	public int getNumberOfWinners(int runnerCount)
	{
		int numberOfWinners = 0;
		if (runnerCount <=7)
		{
			numberOfWinners = 2;
		}
		else if (runnerCount >= 8 && runnerCount <= 15)
		{
			numberOfWinners = 3;
		}
		else
		{
			numberOfWinners = 4;
		}		
		return numberOfWinners;		
	}

	public int runnerDistribution()
	   {
		   Integer index = random.nextInt(100) + 1; 
		   Integer runnerCount = 0;
		
		   if (isBetween(index, 1, 4)) 
		   {
			   runnerCount = 25;
		   } 
		   else if (isBetween(index, 5, 10)) 
		   {
			   runnerCount = 23;
		   }
		   else if (isBetween(index, 6, 7)) 
		   {
			   runnerCount = 21;
		   }
		   else if (isBetween(index, 8, 14)) 
		   {
			   runnerCount = 18;
		   }
		   else if (isBetween(index, 15, 53)) 
		   {
			   runnerCount = 10;
		   }
		   else if (isBetween(index, 54, 72)) 
		   {
			   runnerCount = 12;
		   }
		   else if (isBetween(index, 73, 84)) 
		   {
			   runnerCount = 7;
		   }
		   else if (isBetween(index, 85, 92)) 
		   {
			   runnerCount = 6;
		   }
		   else if (isBetween(index, 93, 100)) 
		   {
			   runnerCount = 8;
		   }
		   return runnerCount;
	   }

	   public static boolean isBetween(int x, int lower, int upper) {
		   return lower <= x && x <= upper;
		 }
}