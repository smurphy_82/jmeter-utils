package ju;

import java.util.concurrent.LinkedBlockingQueue;

public class QueueEventGrade
{
    private static LinkedBlockingQueue<Object> q = new LinkedBlockingQueue<Object>();
    public static void put(Object o) throws Exception{
        q.put(o);
    }
    public static Object get() throws Exception{
        return q.peek();
    }
    public static Object pop() throws Exception{
        return q.poll();
    }    
    public static void clear() throws Exception{
        q.clear();
    }
    public static int size() throws Exception{
    	return q.size();
    }
}