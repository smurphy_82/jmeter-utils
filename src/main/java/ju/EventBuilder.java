package ju;
import java.util.*;

/*
 * Creates event details required for event creation
 */  
public class EventBuilder 
{
   ArrayList<EventCreationDetails> ecds;
   RunnerHelper rh = new RunnerHelper();
   Integer startTimeOffset;
   Integer startTimeIncrement;
   Integer numberOfEvents;
   
   public EventBuilder(Integer startTimeOffset, Integer startTimeIncrement, Integer numberOfEvents)
   {	   
	   ecds = new ArrayList<EventCreationDetails>();
	   this.startTimeOffset = startTimeOffset;
	   this.startTimeIncrement = startTimeIncrement;
	   this.numberOfEvents = numberOfEvents;	   	   
   }   
   
   public EventCreationDetails buildHorseRacingEvent()
   {
	   EventCreationDetails ecd = new EventCreationDetails();
	   ecd.setDisplayDate();
	   ecd.setDisplayTime(setStartTime());
	   ecd.setStartDate(setStartTime());
	   ecd.setName(HorseRacingLocations.KEMPTON.replaceAll("#", ecd.displayTime));
	   ecd.setNumberOfRunners();
	   ecd.setNumberOfWinners();

	   return ecd;
   }
   
   public EventCreationDetails buildSoccerEvent()
   {
	   EventCreationDetails ecd = new EventCreationDetails();
	   ecd.setDisplayDate();
	   ecd.setStartDate(setStartTime());

	   return ecd;
   }
   
   public void addEventCreationDetailsToList(EventCreationDetails eventCreationDetails)
   {
	   this.ecds.add(eventCreationDetails);
   }
   
   public void clearCreationDetailsList()
   {
	   this.ecds.clear();;
   }
   
   public ArrayList<EventCreationDetails> getEventCreationDetailsList()
   {
	   return this.ecds;
   }
     
   public Integer setStartTime()
   {
	   return this.startTimeOffset += startTimeIncrement;
   }
   
}